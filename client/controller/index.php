<?php

// On démarre les sessions
session_start();

// Le modèle
require("../../model/model.php");


// Le contrôleur

$userID = ""; // The ID of the User if he is connected
$nom = "";
$genre = "";
$type = "";
$date = "";
$isAdmin = "";

if (isset($_SESSION["loginUser"])){
    $accountInformations = getAccountInfo($_SESSION["loginUser"]);// Get All informations about the owner of the account
    foreach ($accountInformations AS $getID){
        $userID=$getID->Compte_ID;
        $isAdmin=$getID->Compte_IsAdmin;
}
}
// Obtention des variables POST du formulaire de recherche
if (isset($_POST["genre"]) || isset($_POST["type"]) || isset($_POST["date"])) {
    $nom = htmlspecialchars($_POST["nom"]);
    $genre = htmlspecialchars($_POST["genre"]);
    $type = htmlspecialchars($_POST["type"]);
    $date = htmlspecialchars($_POST["date"]);
}

// Obtention de la liste
// DATE MUST BE DESC OR ASC
$results = getOeuvres($nom, $genre, $type, $date, $isAdmin);

// La view
require ("../../client/view/index.php");