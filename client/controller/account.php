<?php
session_start();
// Le modèle
require("../../model/model.php");



$accountInformations = null; // All the informations about the account owner
$postNumbers = null; // Nmbr of messages posted over all Oeuvres
$publications = null; // Oeuvre_ID + Oeuvre_Name shared
$publicationsNumber = null; // Nmbr of stuff shared
$pendingPublicationsNumber = null;
$publicationAverageReceived = null; //Average of received note (0-5) in the comments for all prefabs posted
$publicationAverageGiven = null; //Average of given note (0-5) by comments
$actualViewedAccount = htmlspecialchars($_GET["id"]);
$isTheViewerOwner = false; // A bool to set if the viewer gonna have the admin tools
$allPublicationOfTheOwner = null; // List all of the publication ID of the owner of the account
$error = null; // Is there a problem ?


// PARTIE REDIRECTION/CONNECTION/PROPRIETEE DU COMPTE

// SI LA PERSONNE EST CONNECTEE ELLE PEUT VOIR LE PROFIL, ON ETABLIS LES VARIABLES
if (isset($_SESSION["loginUser"]) && ($_SESSION["loginUser"] != null)) {

    $accountInformations = getAccountInfo($actualViewedAccount);// Get All informations about the owner of the account

    $postNumbers = getPostNumber($actualViewedAccount);
    $publicationsNumber = getPublicationsNumber($actualViewedAccount);
    $publicationsNumber = count($publicationsNumber);
    $pendingPublicationsNumber = getALLPublicationsNumber($actualViewedAccount);
    $pendingPublicationsNumber = count($pendingPublicationsNumber) - $publicationsNumber;

    //We are finding every note given on every publication by the owner
    //Then we convert the result to a proper array to send it to the function
    //Supposed to do the average by converting all the information to an unique float between 0-5.
    $publicationAverageGiven = publicationAllRatingGiven($actualViewedAccount);
    $averageRatingArray = array();
    foreach ($publicationAverageGiven AS $rating){
        array_push($averageRatingArray, (int)$rating->Commentaire_Note);
    }
    $publicationAverageGiven = makeAverageFromRating($averageRatingArray);


    //Now we do the same for the average of notation received for all the publication shared by the owner
    //In order to get them we must first
    //1) take all the publication ID of the owner,
    //2) request all the notations for this publications ID
    //3) do the average
    $allPublicationOfTheOwner = getAllPublicationIDByUsername($actualViewedAccount);//1
    $averageRatingArrayForAllPublication = array();
    foreach ($allPublicationOfTheOwner AS $publicationID) {//1
        $publicationID = (int)$publicationID[0];//1 We have all publications now with the foreach as INT

        $averageRating = publicationGlobalRating($publicationID);//2 We get the Rating of the publication by the ID collected
        if (isset($averageRating) && ($averageRating != null)) {//Si y'a pas de notation sur la publication on saute la boucle
            $averageRatingArray = array();//2
            foreach ($averageRating AS $rating) {//2
                    array_push($averageRatingArray, (int)$rating->Commentaire_Note);//2
            }//2
            $averageRating = makeAverageFromRating($averageRatingArray);
            //2 Now we have the Average of one of the publication
            //We must push it into the array with the average of ALL the publications from the owner
            array_push($averageRatingArrayForAllPublication, $averageRating);//3 We collect all the averages
        }
    }//3 We make the ultimate average of all the averages
    $publicationAverageReceived = makeAverageFromRating($averageRatingArrayForAllPublication);

    //Si jamais le personne visitant le profil EST AUSSI le propriétaire du profil
    //On l'envoit sur sa page d'administration
    if (isset($actualViewedAccount) && (($actualViewedAccount == ($_SESSION["loginUser"])))) {
        // Si jamais le propriétaire du compte a fait une modification
        if (isset($_GET["isformsubmited"]) && $_GET["isformsubmited"] == 1){
            // Si les champs de modification du password sont vide on envoit sans verification
            //On protège la base de donnée : htmlspecialchar

            $oldpassword = htmlspecialchars($_GET["formpasswordold"]);
            $newpassword = htmlspecialchars($_GET["formpasswordnew"]);
            $newpseudo = htmlspecialchars($_GET["formtitle"]);
            $image = htmlspecialchars($_GET["formimage"]);
            $description = htmlspecialchars($_GET["formdescription"]);

            // Is the pseudo already in the database ?
            if ($newpseudo != "" && getAccountInfo($newpseudo) != null){
                $error = 1;
                header("Location: ../../client/controller/account.php?id=$actualViewedAccount&pseudo=Error");
            }


            if ($oldpassword == "" && $newpassword == "" && $error != 1){

                //Après la modification on redirige automatiquement
                //On passe toutes les valeurs du formulaire. Le tri est fait avec PDO objet
                updateAccount($newpseudo,$image,$description,$newpassword,$actualViewedAccount);
                //Si on a modifié le nom on redirige sur le nouveau nom (Oui ça aurait été mieux
                //De mettre un ID, D'ACCORD ! Mais la session était plus pratique avec le pseudo unique
                if ($newpseudo != ""){
                    //On change le nom de la session
                    $_SESSION["loginUser"] = $newpseudo;
                    header("Location: ../../client/controller/account.php?id=$newpseudo");
                    //Sinon si on ne change pas le nom de l'utilisateur on redirige sur le compte normal
                }else { header("Location: ../../client/controller/account.php?id=$actualViewedAccount"); }
            }
            //Si les champs de modification du password sont les deux remplis on verifie avant d'envoyer
            //Le cas ou l'un ou l'autre est remplis seulement n'est pas pris en compte
            //On ignore si ça arrive, et on envoit aucune modification, c'est au formulaire de gerer ce cas
            if (($oldpassword != "") && ($newpassword != "") && $error != 1) {
                if (verifyPassword($oldpassword, $actualViewedAccount) == true) {
                    //On passe toutes les valeurs du formulaire. Le tri est fait avec PDO objet
                    updateAccount($newpseudo, $image, $description, $newpassword, $actualViewedAccount);
                    if ($newpseudo != ""){
                        //Si on a modifié le nom on redirige sur le nouveau nom en changeant la session
                        $_SESSION["loginUser"] = $newpseudo;
                        header("Location: ../../client/controller/account.php?id=$newpseudo");
                        //Sinon si on ne change pas le nom de l'utilisateur on redirige sur le compte normal
                    }else { header("Location: ../../client/controller/account.php?id=$actualViewedAccount"); }
                }
            }
        }
        $isTheViewerOwner = true; // In the view we are checking this bool to add the dev tools on
        //the profil for the owner
        require ("../../client/view/account.php");

    }
    else {
        //Si la personne est connectée mais n'est pas le proprio du compte.
        //On affiche la page de profil style visiteur sans les fonctions de modification
        require ("../../client/view/account.php");
    }
}
//Si la personne n'est pas connectée, elle ne peut pas stalker les comptes des autres
//Si l'utilisateur n'a pas lancé une session on redirige vers le login pour se connecter
else {
    header("Location: ../../share/controller/login.php");
}
