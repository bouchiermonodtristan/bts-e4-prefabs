<?php
session_start();
// Le modèle
require("../../model/model.php");

// Le contrôleur
// On regarde comment a été appellé la page
$oeuvre_id = -1; // The actual id=???
$actualUser = null; //The userName
$userID = null; //The User ID
$isOwner = false; //It's true if the owner if actualy the one triggering the page
$isAdmin = false;
//OR IF THE ACCOUNT IS ADMIN
$ownerIsModifying = htmlspecialchars($_GET["ownerIsModifying"]); // If the owner clicked on the want to modify button
$userName = htmlspecialchars($_SESSION["loginUser"]); //Another UserName disappearing in a function
$resultsContents = null; // Everything about the view prefab
$resultsComments = null; // Everything about all ppl and the content of the comments for the view prefab
$averageRating = null; // The average rating of the publication
$isOeuvreValidated = null; // Is this oeuvre already setup as a validate oeuvre by an admin ?


if (isset($_GET['id']) && !empty($_GET['id'])) {
    $userNames = getAccountInfo($userName);
    $oeuvre_id = htmlspecialchars($_GET['id']);
    // L'id doit être une valeur numérique
    if (is_numeric($oeuvre_id)) {
        $isOeuvreValidated = getReleasedStateById($oeuvre_id)[0][0];
        $resultsContents = getOeuvre($oeuvre_id);
        $resultsComments = getComments($oeuvre_id);
        $averageRating = publicationGlobalRating($oeuvre_id);
        $averageRatingArray = array();
        foreach ($averageRating AS $rating){
            array_push($averageRatingArray, (int)$rating->Commentaire_Note);
        }
        $averageRating = makeAverageFromRating($averageRatingArray);
    }
}

//Si on a quelqu'un qui a commenté :
if (isset($_GET["comment"])){
    // On verifie s'il est log et qu'on peut recupérer son ID d'utilisateur
    if (!isset($_SESSION["loginUser"]) || ($_SESSION["loginUser"] == null)){
        header("Location: ../../share/controller/login.php");
    }// Si l'utilisateur a commenté mais n'a pas de compte on le redirige gentillement vers le login
    $note = 0;
    if (isset($_GET["noteIs1"])){
        $note = 1;
    }elseif (isset($_GET["noteIs2"])){
        $note = 2;
    }elseif (isset($_GET["noteIs3"])){
        $note = 3;
    }elseif (isset($_GET["noteIs4"])){
        $note = 4;
    }elseif (isset($_GET["noteIs5"])){
        $note = 5;
    }
    $getIDs = getAccountInfo($_SESSION["loginUser"]);
    foreach ($getIDs AS $getID){
        $actualUser=$getID->Compte_ID;
    }
    $thispage = htmlspecialchars($_GET["id"]);
    $comment = htmlspecialchars($_GET["comment"]);
    $title = htmlspecialchars($_GET["title"]);
    // On insert dansla base de donnée avec le nom de l'utilisateur, le commentaire
    //La note donnée et le get ID est l'ID correspondant à l'oeuvre dans la base de donnée
    //insertIntoDatabaseCommentAndNotation($actualUser, $_GET["id"]);
    insertIntoDatabaseCommentaire($comment, $title, $note);
    $commentaire_id = getLastIdInCommentaire();
    $commentaire_id = ((int)$commentaire_id[0]);
    // On insert dansla base de donnée le dernier commentaire rentré, l'utilisateur et l'ID de l'oeuvre
    // Cela sert a les lier ensemble. Possible utilisation d'un trigger plus tard
    insertIntoDatabaseEcrit((int)$actualUser, $commentaire_id, (int)$thispage);
    updateAccountLastMessageTime((int)$actualUser);

    //Une fois l'ajout du commentaire fait, on reactualise la page en reloadant
    header("Location: ../../client/controller/oeuvre.php?id=$thispage");

}
//On récupère les info de la personne regardant la page via la session
foreach ($userNames AS $userName) {
    $userID = $userName->Compte_ID;
    // SI le compte est admin, alors il est aussi owner et peut modifier
    $isAdmin = $userName->Compte_IsAdmin;
    if (isset($isAdmin) && $isAdmin == 1){
        $isOwner = true;
    }
}
// Si celui qui regarde la page est le detenteur de l'oeuvre
if (isPublicationOwner($userID,$oeuvre_id)){
    //Since it's default false, it make us able inside the view to setup the ability to
    //modify stuff is this is the owner
    $isOwner = true;
}

// Si jamais le propriétaire du compte a fait une modification
if (isset($_GET["isformsubmited"]) && $_GET["isformsubmited"] == 1){
    // Si les champs de modification du password sont vide on envoit sans verification
    //On protège la base de donnée : htmlspecialchar

    $download = htmlspecialchars($_GET["formdownload"]);
    $description = htmlspecialchars($_GET["formdescription"]);
    $image = htmlspecialchars($_GET["formimage"]);
    $title = htmlspecialchars($_GET["formtitle"]);

    //On passe toutes les valeurs du formulaire. Le tri est fait avec PDO objet
    updateOeuvre($download,$image,$description,$title,$oeuvre_id);
    header("Location: ../../client/controller/oeuvre.php?id=$oeuvre_id");

}

//Si une demande de suppression a été faite
//SECURITE : ET que c'est l'auteur de l'oeuvre qui la fait !
if (isset($_GET["deletePublication"]) && $_GET["deletePublication"] == 1 && $isOwner == true){

    //1 on casse le lien entre compte et publication
    deletePublicationPublie($oeuvre_id);
    //2 on casse le lien entre les commentaires et publication par un update
    //On a choisi de garder les commentaires - voir doc, rubrique Politique de suppression
    deletePublicationEcrit($oeuvre_id);
    //3 On supprime la publication de la base de donnée
    deletePublication($oeuvre_id);
    //On ramène sur la page du compte, avec un GET qui enverra un message de confirmation de suppression
    $temp = htmlspecialchars($_SESSION["loginUser"]);
    header("Location: ../../client/controller/account.php?id=$temp&suppression=1");
}

if (isset($_GET["validatePublication"]) && $_GET["validatePublication"] == 1 && $isOwner == true){

    //1 on casse le lien entre compte et publication
    validatePublication($oeuvre_id);
    header("Location: ../../client/controller/index.php");
}

// La view
require ("../../client/view/oeuvre.php");

?>