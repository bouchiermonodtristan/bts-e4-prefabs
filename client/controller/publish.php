<?php

// Le modèle
require("../../model/model.php");


// On démarre les sessions
session_start();
$UserID = htmlspecialchars($_SESSION["loginUser"]);
$accountInformations = null;
$currentAccountID = null;
// Le contrôleur
// Si l'utilisateur est connecté on reste sur la page, sinon redirection vers le login !
if (isset($_SESSION["loginUser"]) && ($_SESSION["loginUser"] != null)) {
    $accountInformations = getAccountInfo($UserID);// Get All informations about the owner of the account
        foreach ($accountInformations as $accountInformation){
            $currentAccountID = $accountInformation->Compte_ID;
        }


    // Si jamais quelqu'un Uppload
        if (isset($_GET["isformsubmited"]) && $_GET["isformsubmited"] == 1) {

            $download = htmlspecialchars($_GET["formdownload"]);
            $image = htmlspecialchars($_GET["formimage"]);
            $title = htmlspecialchars($_GET["formtitle"]);
            $description = htmlspecialchars($_GET["formdescription"]);
            $genre = htmlspecialchars($_GET["formgenre"]);
            $type = htmlspecialchars($_GET["formtype"]);


            sendAndPublish($download,$image,$title,$description,$genre,$type);
            $publicationID = getLastPublicationID();

            $publicationID = (int)$publicationID[0];
            insertIntoDatabasePublie($publicationID,$currentAccountID);

            header("Location: ../../client/controller/oeuvre.php?id=$publicationID");

        }













}
else{
    header("Location: ../../share/controller/login.php");
}


// La view
require ("../../client/view/publish.php");