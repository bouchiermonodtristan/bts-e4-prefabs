<?php

// Le modèle
require("../../model/model.php");


// On démarre les sessions
session_start();
$isPasswordWrong = false;
// Le contrôleur
// Si l'utilisateur est connecté il a déjà une session ce con !
//On le redirige vers sa page de compte par ce que faut pas pousser
if (isset($_SESSION["loginUser"]) && ($_SESSION["loginUser"] != null)) {
    header("Location: ../../client/controller/account.php?id=$UserID");
}


    // Si jamais quelqu'un creer un compte
    if (isset($_GET["isformsubmited"]) && $_GET["isformsubmited"] == 1) {
        //we protect but most important, we attack!
        $username = htmlspecialchars($_GET["formtitle"]);
        $image = htmlspecialchars($_GET["formimage"]);
        $password = htmlspecialchars($_GET["formpassword1"]);
        $password2 = htmlspecialchars($_GET["formpassword2"]);
        $description = htmlspecialchars($_GET["formdescription"]);

    if($password === $password2){
            /* DO THE MAIN CODE HERE */

        createNewAccount($username,$image,$password,$description);

        //Since that's the first time this guy log in, he'll be redirected to his fresh profile after login
        //Instead of going back to the index
        header("Location: ../../share/controller/login.php?redirect=$username");
    }else{
        //If the owner used two differents passwords
        $isPasswordWrong = true;
    }
}
















// La view
require ("../../client/view/createaccount.php");