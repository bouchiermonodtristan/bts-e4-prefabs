
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="../../client/controller/index.php">Index</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!-- menu tab 1 -->
            <ul class="nav navbar-nav" style="margin-top: 0.5em">
                 <a href='../../client/controller/publish.php' class='btn btn-primary'>Submit a new creation</a>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php
                if (isset($_SESSION["loginUser"]) && !empty(["loginUser"])) {
                    $user = $_SESSION["loginUser"];
                    echo "<li><a href='../../client/controller/account.php?id=$user'>Hi, $user  </span><i class=\"fa fa-user\" aria-hidden=\"true\"></i>&nbsp;My Account
                    <span id=\"myAccount\" class=\"badge\">0</span></a></li>";
                    echo "<li><a href='../../share/controller/logout.php'><span class='glyphicon glyphicon-off'></span>&nbsp;Déconnexion</a></li>";
                }
                else {
                    echo "<li><a href='../../share/controller/login.php'>Connection</a></li>
                          <li><a href='../../client/controller/createaccount.php'>Register</a></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</nav>

