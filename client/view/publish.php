<!DOCTYPE html>
<html>
<head>
    <title>Account</title>
    <link href="../script/header.css" rel="stylesheet">
    <?php
    include_once ("../../html/header.inc.html");
    ?>
</head>

<body>

<?php
include_once("../../client/html/menu.inc.php");

//On recupere toutes les informations concernant le profil visité
if ($accountInformations){
    foreach ($accountInformations AS $accountInformation){
        ?>
        <form action="../../client/controller/publish.php" method="get" class="form-inline" id="upload">
            <input type="hidden" name="id" value="<?=$UserID?>">
            <input type="hidden" name="isformsubmited" value="1">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <?php
                                    echo"<h3 class=\"panel-title\">$accountInformation->Compte_Pseudo<span class='pull-right'>Uppload your creation </class></h3>";
                                ?>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" align="center">
                                        <?= "<a href='../../client/controller/account.php?id=$accountInformation->Compte_Pseudo' title=\"See profile\"><img src='$accountInformation->Compte_Image'  class=\"img-responsive img-rounded zoomprofilpic\"></a>"; ?>
                                    </div>
                                    <div class=" col-md-9 col-lg-9 ">
                                        <table class="table table-user-information">
                                            <tbody>
                                            <tr>
                                                <?php
                                                    echo"
                                        <div class=\"col-md-3\">
                                        <td>Qu'est-ce ?</td>
                                        <td>
                                         <select class=\"form-control\" id=\"formtype\" name='formtype' form='upload' required>
                                            <option>Prefab</option>
                                            <option>Shader</option>
                                            <option>Model</option>
                                            <option>Effect</option>
                                            <option>Other</option>
                                          </select>
                                          </td>
                                        </div>
                                        </tr>
                                        <tr>
                                      <div class=\"col-md-3\">
                                        <td>Quel support ?</td>
                                        <td>
                                          <select class=\"form-control\" id=\"formgenre\" name='formgenre' form='upload' required>
                                            <option>Unity</option>
                                            <option>VRChat</option>
                                            <option>Unreal</option>
                                            <option>Other</option>
                                          </select>
                                        </td>
                                      </div>
                                      </tr>
                                      <tr>
                                        <td>URL Download :</td>
                                        <td>
                                        <input type=\"text\" class=\"form-control\" id=\"formdownload\" aria-describedby=\"textHelp\" name='formdownload' form='upload' placeholder=\"http://www.download.com/clickonme.ddl\" required>
                                        </td>
                                        </div>
                                        </tr>
                                        <tr>
                                      <div class=\"col-md-3\">
                                        <td>URL Image :</td>
                                        <td>
                                        <input type=\"text\" class=\"form-control\" id=\"formimage\" name='formimage' form='upload' aria-describedby=\"textHelp\" placeholder=\"http://www.image.com/clickonme.png\" required>
                                        </td>
                                      </div>
                                      </tr>
                                      <tr>
                                      <div class=\"col-md-3\">
                                        <td>Title/Name :</td>
                                        <td>
                                        <input type=\"text\" class=\"form-control\" id=\"formtitle\" name='formtitle' form='upload' aria-describedby=\"textHelp\" placeholder=\"Title/Name\" required>
                                        </td>
                                      </div>
                                      </tr>
                                       <tr>
                                      <div class=\"col-md-3\">
                                        <td>Description :</td>
                                        <td>
                                        <textarea rows='7' cols='25' class=\"form-control\" id=\"formdescription\" name='formdescription' form='upload' aria-describedby=\"textHelp\" placeholder=\"Describe your prefab here\" required></textarea>
                                        </td>
                                      </div>";?>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <?php echo "
                            <span class='pull-right'>
                                <input type=\"submit\" value=\"Validate the publication\" class='btn btn-sm btn-primary'>
                            </span>
                            <span>
                                <a href='../../client/controller/account.php?id=$UserID' class='btn btn-sm btn-primary'>Back to the profile</a>
                            </span>
                            ";?>
                                <!-- <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                 -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }
}
?>
