<!DOCTYPE html>
<html>
<head>
    <title>Account</title>
    <link href="../script/header.css" rel="stylesheet">
    <?php
    include_once ("../../html/header.inc.html");
    ?>
</head>

<body>

<?php
include_once("../../client/html/menu.inc.php");

//On recupere toutes les informations concernant le profil visité
if ($accountInformations){
    foreach ($accountInformations AS $accountInformation){
        ?>
        <form action="../../client/controller/account.php" method="get" class="form-inline" id="editprofil">
            <input type="hidden" name="id" value="<?=$_GET["id"]?>">
            <input type="hidden" name="isformsubmited" value="1">
            <div class="container">
                <div class="row">
                    <!-- if this bool is true, then the viewer is also the profil owner so it has the tools -->
                    <?php if ($isTheViewerOwner == true){}?>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <?php if ($isTheViewerOwner == true && $_GET["edit"] == true){
                                    echo"<h3 class=\"panel-title\"><input type=\"text\" class=\"form-control\" id=\"formtitle\" name='formtitle' form='editprofil' aria-describedby=\"textHelp\" placeholder=\"$accountInformation->Compte_Pseudo\">";
                                }else{
                                    echo"<h3 class=\"panel-title\">$accountInformation->Compte_Pseudo";
                                }?>
                                <?php if(isset($_GET["suppression"]) && $_GET["suppression"]==1){echo "<button class='btn btn-danger' style='margin-left: 10em'>You removed a publication</button></h3>";
                                }else if(isset($_GET["pseudo"]) && $_GET["pseudo"]=="Error"){echo "<button class='btn btn-danger' style='margin-left: 10em'>This name already exist</button></h3>";
                                }else
                                { echo
                                "</h3>";}?>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3" align="center">

                                        <?= "<a href='../../client/controller/account.php?id=$accountInformation->Compte_Pseudo' title=\"See profile\"><img src='$accountInformation->Compte_Image'  class=\"img-responsive img-rounded zoomprofilpic\"></a>";
                                        if ($isTheViewerOwner == true && $_GET["edit"] == true){
                                            echo"
                                     <td>
                                     <br>Image URL :
                                      <div class=\"input-group\">
                                     <input type=\"text\" class=\"col-xs-3 form-control\" id=\"formimage\" name='formimage' form='editprofil' aria-describedby=\"textHelp\" placeholder=\"$accountInformation->Compte_Image\">
                                     </div>
                                     </td>
                                        ";}?>
                                    </div>

                                    <div class=" col-md-9 col-lg-9 ">
                                        <table class="table table-user-information">
                                            <tbody>
                                            <tr>
                                                <td>Inscription date:</td>
                                                <td><?= $accountInformation->Compte_DateInscription ?></td>
                                            </tr>
                                            <tr>
                                                <td>Last message:</td>
                                                <td><?= $accountInformation->Compte_DateDernierMessage ?></td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <td>Shared prefabs:</td>
                                                <td><?= $publicationsNumber ?></td>
                                            </tr>
                                            <tr>
                                                <td>Pending prefabs:</td>
                                                <td><?= $pendingPublicationsNumber ?></td>
                                            </tr>
                                            <tr>
                                                <td>Posted messages:</td>
                                                <td><?= count($postNumbers) ?></td>
                                            </tr>
                                            <tr>
                                                <td>Average rating given:</td>
                                                <td>
                                                    <div class="review-block-rate">
                                                        <?php
                                                        $notes = getImageNumberFromNote($publicationAverageGiven);
                                                        // Est-ce que l'utilisateur a noté ?
                                                        if ($notes) {?>
                                                            <button type="button" <?= putStarOnOrOff($notes[0]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notes[1]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notes[2]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notes[3]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notes[4]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                        <?php } ?>
                                                        <span>(<?= round($publicationAverageGiven, 2) ?>)</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Average rating received:</td>
                                                <td>
                                                    <div class="review-block-rate">
                                                        <?php
                                                        $notesreceived = getImageNumberFromNote($publicationAverageReceived);
                                                        // Est-ce que l'utilisateur a noté ?
                                                        if ($notesreceived) {?>
                                                            <button type="button" <?= putStarOnOrOff($notesreceived[0]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notesreceived[1]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notesreceived[2]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notesreceived[3]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                            <button type="button" <?= putStarOnOrOff($notesreceived[4]); ?> aria-label="Left Align">
                                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                            </button>
                                                        <?php } ?>
                                                        <span>(<?= round($publicationAverageReceived, 2) ?>)</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <?php if ($isTheViewerOwner == true && $_GET["edit"] == true){
                                                    echo"
                                        <div class=\"col-md-3\">
                                        <td>Account Description :</td>
                                        <td>
                                        <input type=\"text\" class=\"form-control\" id=\"formdescription\" aria-describedby=\"textHelp\" name='formdescription' form='editprofil' placeholder=\"$accountInformation->Compte_Description\">
                                        </td>
                                        </div>
                                        </tr>
                                        <tr>
                                      <div class=\"col-md-3\">
                                        <td>Old Password :</td>
                                        <td>
                                        <input type=\"text\" class=\"form-control\" id=\"formpasswordold\" name='formpasswordold' form='editprofil' aria-describedby=\"textHelp\" placeholder=\"**************************\">
                                        </td>
                                      </div>
                                      </tr>
                                      <tr>
                                      <div class=\"col-md-3\">
                                        <td>New Password :</td>
                                        <td>
                                        <input type=\"text\" class=\"form-control\" id=\"formpasswordnew\" name='formpasswordnew' form='editprofil' aria-describedby=\"textHelp\" placeholder=\"New Password\">
                                        </td>
                                      </div>";
                                                }
                                                else{
                                                    echo"<td>Account Description:</td>
                                <div class=\"col-md-3\">
                                    <td> $accountInformation->Compte_Description </td>
                                </div>";}?>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <?php if ($isTheViewerOwner == true && $_GET["edit"] != true){ echo "<span class='pull-left'>
                            <a href='../../client/controller/publish.php' class='btn btn-primary'>Submit a new creation</a></span>
                            <span class='pull-right'>
                            <a href='../../client/controller/account.php?id=$actualViewedAccount&edit=true' class='btn btn-primary'>Edition mode profil</a>
                            </span>
                            ";}?>
                                        <?php if ($isTheViewerOwner == true && $_GET["edit"] == true){ echo "<span class='pull-left'>
                            <a href='../../client/controller/publish.php' class='btn btn-primary'>Submit a new creation</a></span>
                            <span class='pull-right'>
                            <input type=\"submit\" value=\"Save the modification\" class='btn btn-primary'>
                            </span>
                            ";}?>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                                <span class="pull-right">
                            <a data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                                    <!-- <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                 --></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }
}
?>
