<!DOCTYPE html>
<html>
<head>
    <title>VRCPrefabs</title>
    <link rel="stylesheet" type="text/css" href="../script/index.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="../script/zoom.css" media="screen" />
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <?php
    include_once ("../../html/header.inc.html");
    ?>
</head>

<body>

<?php
include_once("../../client/html/menu.inc.php");
?>

<h1></h1>

<div class="container body-content">
<!-- Formulaire de recherche -->
<form action="" method="post" class="form-inline">
    <div class="form-group">
        <label for="nom">The name start by :</label>
        <input type="text" class="form-control" name="nom" placeholder="Enter the name" value="<?= $nom ?>"/>
    </div>

    <!-- A PROPOS DU GENRE -->
    <div class="form-group">
        <label for="genre"> Platform :</label>
        <select id="genre" name="genre" class="form-control">
            <option value="" <?php if (empty($genre)) {echo "selected";} ?>>All</option>
            <?php
            $listeGenre = getUniqueGenre();
            foreach ($listeGenre as $ligne)
            {
                echo "<option value='$ligne->state'" . ($genre==$ligne->state ? "selected" : "") . ">$ligne->state</option>";
            }
            ?>
        </select>

    <!-- A PROPOS DU TYPE -->
    </div>
    <div class="form-group">
        <label for="type"> Type :</label>
        <select id="type" name="type" class="form-control">
            <option value="" <?php if (empty($type)) {echo "selected";} ?>>All</option>
            <?php
            $listeType = getUniqueTypes();
            foreach ($listeType as $ligne)
            {
                echo "<option value='$ligne->state'" . ($genre==$ligne->state ? "selected" : "") . ">$ligne->state</option>";
            }
            ?>
        </select>
    </div>
        <!-- A PROPOS DE LA DATE -->
    <div class="form-group">
        <label for="date"> Date :</label>
        <select id="date" name="date" class="form-control">
            <option value="">N/A</option>
            <option value='DESC'>Newest</option>
            <option value='ASC'>Oldest</option>
        </select>
    </div>

    <input type="submit" value="Search" class="btn btn-default" />
</form>
<br >

    <div class="row">
    <?php
    // objet "$publisher" valide ?
    if ($results) {
        foreach ($results as $result)
        {
            // On affiche chaque entrée une à une

            if ($result->Released != 1)//Si c'est en attente de validation background rouge
            {
                ?><figure class="snip1492" style="background-color: #FF5733"><?php
            }
            else
            {
                ?><figure class="snip1492"><?php
            }
            ?>
                <?= "<img src='$result->Oeuvre_Image'  class='img-rounded zoomindex' alt='sample85' />" ?>
                <figcaption>
                    <h3><?= $result->Oeuvre_Titre ?></h3>
                    <p><?=$result->Oeuvre_Genre1 ?> | <?=$result->Oeuvre_Type ?></p>
                    <div class="price">
                        <s>Made By</s><?= $result->Compte_Pseudo ?>
                    </div>
                </figcaption><i class="ion-plus-round"></i>
                <a href='../../client/controller/oeuvre.php?id=<?= $result->Oeuvre_ID ?>' title="Telecharger"></a>
            </figure>
            <?php
        }
    }
    else {
        echo "<tr><td colspan='3'>No result !</td></tr>";
    }
    ?>
    </div>

</div>


</body>
</html>
