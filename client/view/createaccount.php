<!DOCTYPE html>
<html>
<head>
    <title>Account</title>
    <link href="../script/header.css" rel="stylesheet">
    <?php
    include_once ("../../html/header.inc.html");
    ?>
</head>

<body>

<?php
include_once("../../client/html/menu.inc.php");?>


<form action="../../client/controller/createaccount.php" method="get" class="form-inline" id="newaccount">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Name : <input type="text" class="form-control" id="formtitle" name='formtitle' form='newaccount' aria-describedby="textHelp" placeholder="Your name..." required>
                            <span class='pull-right'><a type='button' class='btn btn-danger' >Do not use your VRC password!</a></span>
                        </h3>

                    </div>
                    <!-- Si le formulaire est soumis -->
                    <input type="hidden" name="isformsubmited" value="1">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3" align="center">
                                <a title="Your picture"><img src='https://www.holygrailofficial.com/wp-content/uploads/question-mark-box.jpg'  class="img-responsive img-rounded zoomprofilpic"></a>
                                <br><div class="pull-left">Image URL :</div>
                                    <div class="input-group">
                                        <input type="text" class="col-xs-3 form-control" id="formimage" name='formimage' form='newaccount' aria-describedby="textHelp" placeholder="Image URL" required>
                                    </div>
                            </div>


                            <div class="col-md-9 col-lg-9 ">
                                <table class="table table-user-information">
                                    <tbody>
                                    <tr>
                                        <div class="col-md-3">
                                            <td>Password :</td>
                                            <td>
                                                <input type="password" class="form-control" id="formpassword1" name='formpassword1' form='newaccount' aria-describedby="textHelp" placeholder="*******************" required>
                                            </td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-md-3">
                                            <td>Password Verify :</td>
                                            <td>
                                                <input type="password" class="form-control" id="formpassword2" name='formpassword2' form='newaccount' aria-describedby="textHelp" placeholder="*******************" required>
                                                <?php if($isPasswordWrong == true){echo"<div class='text-danger'> Passwords must matchs !</div>";}?>
                                            </td>

                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-md-3">
                                            <td>Description :</td>
                                            <td>
                                                <textarea rows='7' cols='25' class="form-control" id="formdescription" name='formdescription' form='newaccount' aria-describedby="textHelp" placeholder="Describe yourself here" required></textarea>
                                            </td>
                                        </div>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <?php echo "
                            <span class='pull-right'>
                                <input type=\"submit\" value=\"Validate registry\" class='btn btn-sm btn-primary'>
                            </span>
                            <span>
                                <a href='../../client/controller/index.php' class='btn btn-sm btn-primary'>Cancel registry</a>
                            </span>
                            ";?>
                        <!-- <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                         -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
