<!DOCTYPE html>
<html>
<head>
    <title>Oeuvre</title>
    <link rel="stylesheet" type="text/css" href="../script/zoom.css" media="screen" />
    <?php
    include_once ("../../html/header.inc.html");
    ?>
</head>

<body>

<?php
include_once("../../client/html/menu.inc.php");
?>


<!-- Partie auteur de l'ouvrage -->
<div class="container body-content">

    <div class="row">
        <?php
        // objet "$publisher" valide ?
        if ($resultsContents) {
        foreach ($resultsContents as $resultContent)
        {

        // On affiche chaque entrée une à une
        //Si c'est un admin et que l'oeuvre est pas validée on propose la validation.
        if(($isAdmin == true) && ($isOeuvreValidated != 1)){
            echo"
          <div class=\"container mt-4 mb-4\">
            <div class=\"panel panel-primary\">
                <div class=\"panel-heading\">
                <span><a href='../../client/controller/oeuvre.php?id=$oeuvre_id&validatePublication=1' type='button' class='btn btn-success' style='text-align: center' >Validate this publication</a>
                </span>
                <span><a href='../../client/controller/oeuvre.php?id=$oeuvre_id&deletePublication=1' type='button' class='btn btn-danger' style='text-align: center' >Deny this publication</a>
                </span>
                </div>
            </div>
          </div>
                 ";
        }else if(($isAdmin !== true) && ($isOeuvreValidated != 1)){
            echo"
          <div class=\"container mt-4 mb-4\">
            <div class=\"panel panel-primary\">
                <div class=\"panel-heading\">
                <span style='text-align: center'><a href='../../client/controller/oeuvre.php?id=$oeuvre_id' type='button' class='btn btn-danger' >This is a new publication. It's been waiting for moderation before beeing released.</a>
                </span>
                </div>
            </div>
          </div>
                 ";
        }


        // Si le propriétaire de l'oeuvre est entrain de regarder
        // ET si le propriétaire de l'oeuvre a cliqué sur modifier
        if($ownerIsModifying == true && $isOwner == true){
            echo"
            <form action='../../client/controller/oeuvre.php' method='get' class='form-inline' id='update'>
            <input type='hidden' name='isformsubmited' value='1'>";
        }
        ?>


        <div class="container mt-4 mb-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <?php
                    //Si le detenteur de l'ouvre est sur la page et ne veut pas modifier
                    //On propose le bouton pour modifier
                    if($ownerIsModifying != 1 && $isOwner == true){
                        echo "<h1><a href=\"../../client/controller/oeuvre.php?id=$oeuvre_id&ownerIsModifying=1\" data-original-title=\"Edit this publication\" data-toggle=\"tooltip\" type=\"button\" class=\"btn btn-sm-5 btn-warning\" style='margin-bottom: 0.4em;'><i class=\"glyphicon glyphicon-edit\"></i></a>
                                    $resultContent->Oeuvre_Titre<a href='$resultContent->Oeuvre_Telechargement' title='Telecharger' target='blank' ><i class='fa fa-download fa-2x' aria-hidden='false' style='color: #fff; float: right; margin-top: -0.2em;'></i></a></h1>";
                    }
                    //Si le détenteur de l'oeuvre est sur la page et modifie
                    //On propose le champ de modification
                    elseif($ownerIsModifying == 1 && $isOwner == true){
                        echo "<h1><a href=\"../../client/controller/oeuvre.php?id=$oeuvre_id&ownerIsModifying=0\"data-original-title=\"Edit this publication\" data-toggle=\"tooltip\" type=\"button\" class=\"btn btn-sm-5 btn-warning\" ><i class=\"glyphicon glyphicon-arrow-left\"></i></a>
                                  <input type=\"text\" class=\"form-control\" id=\"formtitle\" name='formtitle' form='update' aria-describedby=\"textHelp\" placeholder='$resultContent->Oeuvre_Titre'><i class=\"glyphicon glyphicon-download-alt pull-right\" style='font-size: 0.8em;margin-left: 0.5em;'></i>
                                  <input type=\"text\" class='form-control pull-right' id=\"formdownload\" name='formdownload' form='update' aria-describedby=\"textHelp\" placeholder='$resultContent->Oeuvre_Telechargement'></h1>";
                    }
                    //Si jamais la personne sur la page n'est pas le détenteur de l'oeuvre
                    //On Affiche juste le nom et pas de possibilité de modification
                        else
                        {
                        echo "<h1>$resultContent->Oeuvre_Titre<a href='$resultContent->Oeuvre_Telechargement' title=\"Telecharger\" target=\"_blank\" ><i class=\"fa fa-download fa-2x\" aria-hidden=\"false\" style=\"color: #fff; float: right; margin-top: -0.2em;\"></i></a>
                    </h1>";}?>



                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12">


                            <div class="row">
                                <div class="col-sm-2">

                                    <div class="rating-block" >

                                        <h4>Average user rating</h4>
                                        <h2 class="bold padding-bottom-7"><?= round($averageRating, 2) ?> <small>/ 5</small></h2>
                                        <?php
                                        // Est-ce que l'utilisateur a noté ?
                                        $notes = getImageNumberFromNote($averageRating);
                                        if ($notes) {?>
                                            <div>
                                                <button type="button" <?= putStarOnOrOff($notes[0]); ?> aria-label="Left Align">
                                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" <?= putStarOnOrOff($notes[1]); ?> aria-label="Left Align">
                                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" <?= putStarOnOrOff($notes[2]); ?> aria-label="Left Align">
                                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" <?= putStarOnOrOff($notes[3]); ?> aria-label="Left Align">
                                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                </button>
                                                <button type="button" <?= putStarOnOrOff($notes[4]); ?> aria-label="Left Align">
                                                    <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                                </button>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <!-- Si l'utilisateur a choisi de modifier on lui met l'input de l'URL de l'image en plus -->
                                   <?php if($ownerIsModifying == 1 && $isOwner == true){
                                       echo"<h1><i class=\"glyphicon glyphicon-picture pull-left\" style='font-size: 0.8em;'><input style='margin-left: 0.5em;margin-top: 0.4em' type=\"text\" class='form-control' id=\"formimage\" name='formimage' form='update' aria-describedby=\"textHelp\" placeholder='$resultContent->Oeuvre_Image'></i></h1>
                                        <span><input type=\"submit\" value=\"Save the modifications\" style='margin-top: 3em;margin-left: 3em' class='btn btn-primary'></span>
                                        <span><button type=\"button\" data-toggle=\"modal\" data-target='#myModal' style='margin-top: 1em;margin-left: 3em' class='btn btn-danger'>Delete this publication</button></span>
                                ";} ?>
                                </div>

                                <div class="col-xs-8">
                                    <?= "<img src='$resultContent->Oeuvre_Image'  class=\"img-responsive img-rounded zoom\">" ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= "<a href='../../client/controller/account.php?id=$resultContent->Compte_Pseudo' title=\"See profile\"><img src='$resultContent->Compte_Image'  class=\"img-responsive img-rounded zoomprofilpic\"></a>" ?>
                                    <div class="review-block-name" style="text-align: right; padding-right: 2em"><a href='../../client/controller/account.php?id=<?= $resultContent->Compte_Pseudo ?>' title="See profile"><?= $resultContent->Compte_Pseudo ?></a></div>
                                    <div class="review-block-date" style="text-align: right; padding-right: 2em"><?= "Posted : " . $resultContent->Oeuvre_DateCreation ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span><hr/>
                        <?php if($ownerIsModifying == 1 && $isOwner == true){
                                   echo "<div>
                                        <td>
                                        <textarea type=\"text\" rows='2' cols='120' class=\"form-control\" id=\"formdescription\" aria-describedby=\"textHelp\" name='formdescription' form='update' placeholder=\"$resultContent->Oeuvre_Contenu\"></textarea>
                                        </td>
                                        </div>";
                        }else{
                        echo "$resultContent->Oeuvre_Contenu";
                        }?>
                        <hr/></span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- YOUR COMMENT HERE -->
<div class="container mt-4 mb-4">
    <div class="panel panel-primary">
        <div class="panel-body">
            <form class="form-inline" method="get" action="../controller/oeuvre.php?id=<?= $_GET["id"] ?>" id="submith_comment">
                <div class="form-group">
                    <span>Your rating :</span>
                    <div class="radio">
                        <label><input type="radio" name="noteIs0">0</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="noteIs1">1</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="noteIs2">2</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="noteIs3">3</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="noteIs4">4</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="noteIs5">5</label>
                    </div>
                    <hr/>
                    <div class="review-block">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?=$_GET["id"]?>">
                                    <label for="title">Title:</label>
                                    <textarea rows="2" cols="10" id="title" name="title" form="submith_comment">Title</textarea>
                                    <label for="comment">Comment:</label>
                                    <textarea rows="3" cols="50" id="comment" name="comment" form="submith_comment">Comment here</textarea>
                                </div>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                    </div>
                </div>
            </form>


<hr/>
</div>
</div>
</div>


<!-- PARTIE COMMENTAIRES -->
<?php
// Est-ce qu'on a des commentaires ?
if ($resultsComments) {
    foreach ($resultsComments as $resultComment) {
        // On affiche chaque entrée une à une

        // On recupère le tableau de note pour assigner les icones de notation
        $notes = getImageNumberFromNote($resultComment->Commentaire_Note);
        ?>
        <div class="container mt-4 mb-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4><?= $resultComment->Commentaire_Titre ?></h4>
                </div>

                <div class="panel-body">
                    <hr/>
                    <div class="review-block">
                        <div class="row">
                            <div class="col-sm-3">
                                <?= "<a href='../../client/controller/account.php?id=$resultComment->Compte_Pseudo' title=\"See profile\"><img src='$resultComment->Compte_Image'  class=\"img-responsive img-rounded zoomprofilpic\"></a>" ?>
                                <div class="review-block-name"><a href='../../client/controller/account.php?id=<?= $resultComment->Compte_Pseudo ?>' title="See profile"><?= $resultComment->Compte_Pseudo ?></a></div>
                                <div class="review-block-date"><?= "Posted : " . $resultComment->Commentaire_DatePublication ?></div>
                            </div>
                            <div class="col-sm-9">
                                <div class="review-block-rate">
                                    <?php
                                    // Est-ce que l'utilisateur a noté ?
                                    if ($notes) {?>
                                        <button type="button" <?= putStarOnOrOff($notes[0]); ?> aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" <?= putStarOnOrOff($notes[1]); ?> aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" <?= putStarOnOrOff($notes[2]); ?> aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" <?= putStarOnOrOff($notes[3]); ?> aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" <?= putStarOnOrOff($notes[4]); ?> aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                    <?php } ?>
                                    <span>(<?= $resultComment->Commentaire_Note ?>)</span>
                                </div>
                                <div class="review-block-title"><br><?= $resultComment->Commentaire_Titre ?></div>
                                <div class="review-block-description"><?= $resultComment->Commentaire_Contenu ?></div>
                            </div>
                        </div>
                        <hr/>
                    </div>

                </div>
            </div>
        </div>



        <?php
    }
}
else { echo "<tr><td colspan='3'>0 Comments posted yet</td></tr>";} ?>


    <!-- THE MODAL POPUP WARNING ON CLICK DELETE -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Are you sure to delete this publication ?</h4>
                </div>
                    <div class='col-12 modal-body text-center'>
                    <button type="button" class="btn btn-success" data-dismiss="modal">Put me safe</button>
                    <?= "<a href='../../client/controller/oeuvre.php?id=$oeuvre_id&deletePublication=1' type='button' class='btn btn-danger' >Confirm Delete</a>" ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

<?php
}


}
else { echo "<tr><td colspan='3'>Aucun résultat</td></tr>";} ?>


