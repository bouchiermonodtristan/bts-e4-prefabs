<?php

function getDataBase()
{
    $host = "db734932467.db.1and1.com";
    $dbName = "db734932467";
    $login = "dbo734932467";
    $password = "tfcrdxeswzqaA1&";

    try
    {
        $bdd = new PDO('mysql:host='.$host.';dbname='.$dbName.';charset=utf8', $login, $password, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    catch (Exception $e)
    {
        $bdd = null;
        die('Erreur : ' . $e->getMessage());
    }

    return $bdd;
}

function authenticateUser($login, $password, PDO $bdd = null)
{
    $user = null;

    if ($bdd == null)
    {
        $bdd = getDataBase();
    }

    $connect = false;
    if ($bdd)
    {
        try
        {
            $stmt = $bdd->prepare ("SELECT * FROM Compte WHERE Compte_Pseudo=:pLogin");
            $stmt->bindParam(':pLogin', $login);
            if ($stmt->execute()) {
                $user = $stmt->fetch(PDO::FETCH_OBJ);
                if ($user && $password == $user->Compte_Password/*password_verify($password, $user->Compte_Password)*/) {

                    $connect = true;
                }
            }
        }
        catch (Exception $e)
        {
            $connect = false;
        }
    }
    if ($connect)
        return $user;

    return null;
}
//This function is unused yet, can be used later by the research to sort by TYPE
function getUniqueTypes(PDO $bdd = null)
{
    if ($bdd == null)
    {
        $bdd = getDataBase();
    }

    if ($bdd)
    {

        $query = "SELECT DISTINCT(Oeuvre_Type) as state FROM Oeuvre ORDER BY state";
        $resultset = $bdd->query($query);
        $liste = $resultset->fetchAll(PDO::FETCH_OBJ);
        $resultset->closeCursor();

        return $liste;
    }

    return null;
}

//This function is used by the research to sort by Genre
function getUniqueGenre(PDO $bdd = null)
{
    if ($bdd == null)
    {
        $bdd = getDataBase();
    }

    if ($bdd)
    {

        $query = "SELECT DISTINCT(Oeuvre_Genre1) as state FROM Oeuvre ORDER BY state";
        $resultset = $bdd->query($query);
        $liste = $resultset->fetchAll(PDO::FETCH_OBJ);
        $resultset->closeCursor();

        return $liste;
    }

    return null;
}

//This function return you every prefab, you can sort them by type
function getOeuvres($nom, $genre, $type, $date, $isAdmin, PDO $bdd = null) {
    // DATE MUST BE "DESC" or "ASC"
    $hideNonReleased = 1;//We hide the pending for moderation oeuvres from non admin
    $results = null;

    if (isset($isAdmin) && $isAdmin == 1){
        $hideNonReleased = null;
    }

    if ($bdd == null)
    {
        $bdd = getDataBase();
    }

    if ($bdd)
    {
        // connexion réussie
        // La requete de base
        $query = "SELECT * FROM Oeuvre, Publie, Compte WHERE Oeuvre.Oeuvre_ID = Publie.Oeuvre_ID AND Publie.Compte_ID = Compte.Compte_ID";
        // On récupère tout le contenu de la table
        if (empty($genre) && empty($nom) && empty($type) && empty($date) && ($isAdmin == 1)) {
            // Tous les enregistrements
            $stmt = $bdd->prepare($query);
        } else {
            $queryWhere = "";
            if (!empty($nom)) {
                $queryWhere = " AND Oeuvre_Titre like :pName";
            }
            if (!empty($genre)) {
                $queryWhere .= " AND Oeuvre_Genre1 = :pState";
            }
            if (!empty($type)) {
                $queryWhere .= " AND Oeuvre_Type = :pType";
            }
            if (!empty($hideNonReleased)) {
                $queryWhere .= " AND Released = :pStatus";
            }
            if (!empty($date)) {
                $queryWhere .= " ORDER BY Oeuvre.Oeuvre_ID";
                $queryWhere .= " $date";
            }
            // Concatenation de la requete
            $query = $query . $queryWhere;
            $stmt = $bdd->prepare($query);
            if (!empty($nom)) {
                $nom = $nom . "%";
                $stmt->bindParam(':pName', $nom);
            }
            if (!empty($genre)) {
                $stmt->bindParam(':pState', $genre);
            }
            if (!empty($type)) {
                $stmt->bindParam(':pType', $type);
            }
            if (!empty($hideNonReleased)) {
                $stmt->bindParam(':pStatus', $hideNonReleased);
            }
        }
        $stmt->execute();

        $results = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $results;
}

// This function return you everything about the author and the stuff from the prefab ID
function getOeuvre($id, PDO $bdd = null)
{
    $result = null;

    if ($bdd == null) {
        $bdd = getDataBase();
    }

    // La bd est-elle valide ?
    if ($bdd) {

        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT * FROM Oeuvre AS o, Publie AS p, Compte AS c WHERE o.Oeuvre_ID = p.Oeuvre_ID AND p.Compte_ID = c.Compte_ID AND o.Oeuvre_ID = :pId";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pId', $id);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

    }
    return $result;
}

// This function return you every comments from users for the said ID
//It give you, comment Title, Date, Content, Note and user Pseudo,and ID to click on it
function getComments($id, PDO $bdd = null)
{
    $result = null;

    if ($bdd == null) {
        $bdd = getDataBase();
    }

    // La bd est-elle valide ?
    if ($bdd) {

        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT co.Commentaire_Titre, co.Commentaire_DatePublication, co.Commentaire_Contenu, co.Commentaire_Note, c.Compte_Pseudo, c.Compte_ID, c.Compte_Image
 FROM Oeuvre AS o, Commentaire AS co, Compte AS c, Ecrit AS e WHERE o.Oeuvre_ID = e.Oeuvre_ID AND e.Compte_ID = c.Compte_ID AND e.Commentaire_ID = co.Commentaire_ID AND o.Oeuvre_ID = :pId";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pId', $id);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

    }
    return $result;
}


function getTitles($title, $type,PDO $bdd = null) {
    $titles = null;

    if ($bdd == null)
    {
        $bdd = getDataBase();
    }
    if ($bdd)
    {
        // connexion réussie
        // La requete de base
        $query = "SELECT * FROM titles WHERE title = title";
        // On récupère tout le contenu de la table
        if (empty($type) && empty($title)) {
            // Tous les enregistrements
            $stmt = $bdd->prepare($query);
        } else {
            $queryWhere = "";
            if (!empty($title)) {
                $queryWhere = " AND title like :pName";
            }
            if (!empty($type)) {
                $queryWhere .= " AND type like :pState";
            }
            // Concanetation de la requete
            $query = $query . $queryWhere;
            $stmt = $bdd->prepare($query);
            if (!empty($title)) {
                $title = $title . "%";
                $stmt->bindParam(':pName', $title);
            }
            if (!empty($type)) {
                $stmt->bindParam(':pState', $type);
            }
        }
        $stmt->execute();
        $titles = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $titles;
}


//It return everything from a account pseudo (They are uniques)
function getAccountInfo($accountName, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT * FROM Compte WHERE Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $result;
}

// Get the number and the links and the name of the shared prefabs by the owner
function getPublicationsInfo($accountName, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT o.Oeuvre_ID, o.Oeuvre_Titre FROM Compte AS c, Publie AS p, Oeuvre AS o
WHERE c.Compte_ID = p.Compte_ID AND p.Oeuvre_ID = o.Oeuvre_ID AND Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $result;
}


function insertIntoDatabasePublie($oeuvre_id, $account_id, PDO $bdd = null)
{
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {
            // Insertion dans la bd
            $query = "INSERT INTO Publie(Compte_ID, Oeuvre_ID) VALUE($account_id, $oeuvre_id)";
            $stmt = $bdd->prepare($query);
            $stmt->execute();
        }
        catch (Exception $e)
        {
        }
    }
}

function insertIntoDatabaseEcrit($account_id, $commentaire_id, $oeuvre_id, PDO $bdd = null)
    {
        if ($bdd == null) {
            $bdd = getDataBase();
        }
        // La bd est-elle valide ?
        if ($bdd) {
            try
            {
                // Insertion dans la bd
                $query = "INSERT INTO Ecrit(Compte_ID, Commentaire_ID, Oeuvre_ID) VALUE($account_id, $commentaire_id, $oeuvre_id)";
                $stmt = $bdd->prepare($query);
                $stmt->execute();
            }
            catch (Exception $e)
            {
            }
        }
    }

function createNewAccount($username, $image, $password, $description, PDO $bdd = null)
{
    $nbModifs = 0;
    $date = date('Y-m-d H:i:s');
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {
            // Insertion dans la bd
            $stmt = $bdd->prepare ('INSERT INTO Compte(Compte_Pseudo, Compte_DateInscription, Compte_Password, Compte_Description, Compte_Image)
VALUE(:pUsername, :pDate, :pPassword, :pDescription, :pImage)');
            $stmt->bindParam(':pUsername', $username, PDO::PARAM_STR);
            $stmt->bindParam(':pImage', $image, PDO::PARAM_STR);
            $stmt->bindParam(':pPassword', $password, PDO::PARAM_STR);
            $stmt->bindParam(':pDescription', $description, PDO::PARAM_STR);
            $stmt->bindParam(':pDate', $date, PDO::PARAM_STR);

            $nbModifs = $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function insertIntoDatabaseCommentaire($comment, $comment_title, $note, PDO $bdd = null)
{
    $nbModifs = 0;
    $date = date('Y-m-d H:i:s');
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {
            // Insertion dans la bd
            $stmt = $bdd->prepare ('INSERT INTO Commentaire(Commentaire_Titre, Commentaire_DatePublication, Commentaire_Contenu, Commentaire_Note) VALUE(:pCommentTitle, :pDate, :pComment, :pNote)');
            $stmt->bindParam(':pComment', $comment, PDO::PARAM_STR);
            $stmt->bindParam(':pDate', $date, PDO::PARAM_STR);
            $stmt->bindParam(':pCommentTitle', $comment_title, PDO::PARAM_STR);
            $stmt->bindParam(':pNote', $note, PDO::PARAM_INT);
            $nbModifs = $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function sendAndPublish($download, $image, $title, $description, $genre, $type, PDO $bdd = null)
{
    $nbModifs = 0;
    $date = date('Y-m-d H:i:s');
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {

            // Insertion dans la bd
            $stmt = $bdd->prepare ("INSERT INTO Oeuvre(Oeuvre_Telechargement, Oeuvre_Image, Oeuvre_Titre, Oeuvre_Contenu, Oeuvre_DateCreation, Oeuvre_Genre1, Oeuvre_Type) VALUE(:pDownload,:pImage,:pTitle,:pDescription,:pDate,:pGenre,:pOeuvreType)");
            $stmt->bindParam(':pDownload', $download, PDO::PARAM_STR);
            $stmt->bindParam(':pImage', $image, PDO::PARAM_STR);
            $stmt->bindParam(':pTitle', $title, PDO::PARAM_STR);
            $stmt->bindParam(':pDescription', $description, PDO::PARAM_STR);
            $stmt->bindParam(':pDate', $date, PDO::PARAM_STR);
            $stmt->bindParam(':pGenre', $genre, PDO::PARAM_STR);
            $stmt->bindParam(':pOeuvreType', $type, PDO::PARAM_STR);
            $nbModifs = $stmt->execute();

        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function updateAccountLastMessageTime($user_id, PDO $bdd = null){
    $nbModifs = 0;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    $date = date('Y-m-d H:i:s');
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {
            // Mise à jour dans la bd
            $stmt = $bdd->prepare ("UPDATE Compte SET Compte_DateDernierMessage=:pDate WHERE Compte_ID=:pIdUser");
            $stmt->bindParam(':pDate', $date, PDO::PARAM_STR);
            $stmt->bindParam(':pIdUser', $user_id, PDO::PARAM_INT);
            $nbModifs = $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function getLastIdInCommentaire(PDO $bdd = null){
    {
        $result = null;
        if ($bdd == null) {
            $bdd = getDataBase();
        }
        // La bd est-elle valide ?
        if ($bdd) {
            // connexion réussie
            // 3 - On récupère toutes les données de l'éditeur
            $query = "SELECT MAX(Commentaire_ID) FROM Commentaire";
            $stmt = $bdd->prepare($query);
            $stmt->execute();
            $result = $stmt->fetch(PDO::FETCH_NUM);
        }
        return $result;
    }
}
//On recupère toutes les informations modifiable de l'oeuvre !
function updateOeuvre($oeuvre_download, $oeuvre_image, $oeuvre_description, $oeuvre_title, $oeuvre_id, PDO $bdd = null){

    $nbModifs = 0;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {
            // Mise à jour dans la bd
            $stmt =  ("UPDATE Oeuvre SET ");
            //Si le pseudo existe on concatène
            if($oeuvre_download != ""){
                $stmt .= " Oeuvre_Telechargement=:pDownload ";
            }
            //On test pour le 2eme paramètre si le premier existe déjà
            if($oeuvre_image != "" && $oeuvre_download != ""){
                //S'il existe on rajoute une ,
                $stmt .= ", Oeuvre_Image=:pImage ";
            }elseif ($oeuvre_image != "" && $oeuvre_download == ""){
                $stmt .= " Oeuvre_Image=:pImage ";
            }
            if($oeuvre_description != "" && ($oeuvre_image != "" || $oeuvre_download != "")){
                $stmt .= ", Oeuvre_Contenu=:pDescription ";
            }elseif ($oeuvre_description != "" && ($oeuvre_image == "" && $oeuvre_download == "")){
                $stmt .= " Oeuvre_Contenu=:pDescription ";
            }
            if($oeuvre_title != "" && ($oeuvre_image != "" || $oeuvre_download != "" || $oeuvre_description != "")) {
                $stmt .= ", Oeuvre_Titre=:pTitle ";
            }elseif ($oeuvre_title != "" && ($oeuvre_image == "" && $oeuvre_download == "" && $oeuvre_description == "")){
                $stmt .= " Oeuvre_Titre=:pTitle ";
            }
            //On fini la requète en concaténant une dernière fois $stmt
            $stmt .= " WHERE Oeuvre_ID=:pID";
            $stmt = $bdd->prepare($stmt);
            //On bind param avec les parametres existants
            if($oeuvre_download != ""){$stmt->bindParam(':pDownload', $oeuvre_download, PDO::PARAM_STR);}
            if($oeuvre_image != ""){$stmt->bindParam(':pImage', $oeuvre_image, PDO::PARAM_STR);}
            if($oeuvre_description != ""){$stmt->bindParam(':pDescription', $oeuvre_description, PDO::PARAM_STR);}
            if($oeuvre_title != ""){$stmt->bindParam(':pTitle', $oeuvre_title, PDO::PARAM_STR);}
            //Ce parametre sera toujours utilisé puisqu'il s'agit de l'ID d'upload utilisé
            $stmt->bindParam(':pID', $oeuvre_id, PDO::PARAM_STR);
            $nbModifs = $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}
//On recupère toutes les informations modifiable du compte
function updateAccount($compte_pseudo, $compte_image, $compte_description, $compte_password, $compte_ancienpseudo, PDO $bdd = null){

    $nbModifs = 0;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try
        {
            // Mise à jour dans la bd
            $stmt =  ("UPDATE Compte SET ");
            //Si le pseudo existe on concatène
            if($compte_pseudo != ""){
                $stmt .= " Compte_Pseudo=:pPseudo ";
            }
            //On test pour le 2eme paramètre si le premier existe déjà
            if($compte_image != "" && $compte_pseudo != ""){
                //S'il existe on rajoute une ,
                $stmt .= ", Compte_Image=:pImage ";
            }elseif ($compte_image != "" && $compte_pseudo == ""){
                $stmt .= " Compte_Image=:pImage ";
            }
            if($compte_description != "" && ($compte_image != "" || $compte_pseudo != "")){
                $stmt .= ", Compte_Description=:pDescription ";
            }elseif ($compte_description != "" && ($compte_image == "" && $compte_pseudo == "")){
                $stmt .= " Compte_Description=:pDescription ";
            }
            if($compte_password != "" && ($compte_image != "" || $compte_pseudo != "" || $compte_description != "")) {
                $stmt .= ", Compte_Password=:pPassword ";
            }elseif ($compte_password != "" && ($compte_image == "" && $compte_pseudo == "" && $compte_description == "")){
                $stmt .= " Compte_Password=:pPassword ";
            }
            //On fini la requète en concaténant une dernière fois $stmt
            $stmt .= " WHERE Compte_Pseudo=:pID";
            $stmt = $bdd->prepare($stmt);
            //On bind param avec les parametres existants
            if($compte_pseudo != ""){$stmt->bindParam(':pPseudo', $compte_pseudo, PDO::PARAM_STR);}
            if($compte_image != ""){$stmt->bindParam(':pImage', $compte_image, PDO::PARAM_STR);}
            if($compte_description != ""){$stmt->bindParam(':pDescription', $compte_description, PDO::PARAM_STR);}
            if($compte_password != ""){$stmt->bindParam(':pPassword', $compte_password, PDO::PARAM_STR);}
            //Ce parametre sera toujours utilisé puisqu'il s'agit de l'ID d'upload utilisé
            $stmt->bindParam(':pID', $compte_ancienpseudo, PDO::PARAM_STR);
            $nbModifs = $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

//Fonction rapide pour vérifier si le password de la base de donnée
//correspond à celui entré pour le pseudo
function verifyPassword($password, $comptename){
    $oldPassword = null;
    $results = getOldPassword($comptename);
    foreach ($results AS $result) {
        $oldPassword = $result->Compte_Password;
        if($oldPassword == $password){
            return true;
        }
    }
}

//On recupère un mot de passe à partir du pseudo
function getOldPassword($accountName, PDO $bdd = null){
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT Compte_Password FROM Compte WHERE Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $result;
}

function validatePublication($publication_id, PDO $bdd = null){
    $nbModifs = 0;
    $null = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try{
            // connexion réussie
            // 3 - On récupère toutes les données de l'éditeur
            $query = "UPDATE Oeuvre SET Released = 1 WHERE Oeuvre_ID = :pPublication";
            $stmt = $bdd->prepare($query);
            $stmt->bindParam(':pPublication', $publication_id);
            $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function getReleasedStateById($publication_ID, PDO $bdd = null){
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // On Recupère le status validé ou non de la publication
        $query = "SELECT Released FROM Oeuvre WHERE Oeuvre_ID = :pPublication";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pPublication', $publication_ID);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    return $result;
}

/************************************************************************/
/********************** STATISTICS FUNCTIONS ****************************/
/************************************************************************/
//Return the last publication ID
function getLastPublicationID(PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT MAX(Oeuvre_ID) FROM Oeuvre";
        //On recupère tous le dernier enregistrement de publie
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();
        //On fetch une seule fois et on sort en num
        $result = $stmt->fetch(PDO::FETCH_NUM);
    }
    return $result;
}

//It return a number of fetch equal at the number of publication by account name
//Then we gonna use the count function to get the exact publication number
function getPublicationsNumber($accountName, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT o.Oeuvre_ID FROM Oeuvre AS o, Compte AS c, Publie AS p
WHERE c.Compte_ID = p.Compte_ID AND o.Released = 1 AND o.Oeuvre_ID = p.Oeuvre_ID AND c.Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    return $result;
}

function getALLPublicationsNumber($accountName, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT o.Oeuvre_ID FROM Oeuvre AS o, Compte AS c, Publie AS p
WHERE c.Compte_ID = p.Compte_ID AND o.Oeuvre_ID = p.Oeuvre_ID AND c.Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    return $result;
}

//It return a number of fetch equal at the number of post by account name
//Then we gonna use the count function to get the exact post number
function getPostNumber($accountName, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT co.Commentaire_ID FROM Compte AS c, Ecrit AS e, Commentaire AS co
WHERE c.Compte_ID = e.Compte_ID AND e.Commentaire_ID = co.Commentaire_ID AND Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    return $result;
}

function getAllPublicationIDByUsername($accountName, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT o.Oeuvre_ID FROM Compte AS c, Publie AS p, Oeuvre AS o
WHERE o.Oeuvre_ID = p.Oeuvre_ID AND c.Compte_ID = p.Compte_ID AND c.Compte_Pseudo = :pAccountName";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccountName', $accountName);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    return $result;
}

function isPublicationOwner($actualUser,$oeuvre_id){
    $result = getTheOwnerOfAPublication($oeuvre_id);
    if ((int)$result[0] == $actualUser){
        return true;
    }
}

/* MODIFYING */
function isCommentOwner($actualUser,$oeuvre_id){
    $result = getTheOwnerOfAPublication($oeuvre_id);
    if ((int)$result[0] == $actualUser){
        return true;
    }
}

//On recupère l'ID du COMPTE détenteur d'une OEUVRE par l'ID de l'OEUVRE
function getTheOwnerOfAPublication($oeuvre_id, PDO $bdd = null)
{
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT Compte_ID FROM Publie AS p WHERE p.Oeuvre_ID = :pOeuvreID";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pOeuvreID', $oeuvre_id);
        $stmt->execute();

        $result = $stmt->fetch(PDO::FETCH_NUM);
    }
    return $result;
}

// Take the publication ID and give on return all the rating associated
function publicationGlobalRating($OeuvreID, PDO $bdd = null){
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT co.Commentaire_Note FROM Oeuvre AS o, Ecrit AS e, Commentaire AS co
WHERE co.Commentaire_ID = e.Commentaire_ID AND e.Oeuvre_ID = o.Oeuvre_ID AND o.Oeuvre_ID = :pIdOeuvre";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pIdOeuvre', $OeuvreID);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $result;
}

// Take the publication ID and give on return all the rating associated
function publicationAllRatingGiven($accountPseudo, PDO $bdd = null){
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "SELECT co.Commentaire_Note FROM Compte AS c, Ecrit AS e, Commentaire AS co
WHERE co.Commentaire_ID = e.Commentaire_ID AND c.Compte_ID = e.Compte_ID AND c.Compte_Pseudo = :pAccount";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pAccount', $accountPseudo);
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    return $result;
}



//Use it to make the average of the rating
function makeAverageFromRating($a){
    $average = ((array_sum($a))/(count($a)));
    return $average;
}

// This function get the entry note from
function getImageNumberFromNote($note){
    $return = array(0,0,0,0,0);
    if (isset($note) && ($note < 6)){
        if ($note == 0){
            return $return;
        }elseif ($note <= 1){
            $return = array(1,0,0,0,0);
            return $return;
        }elseif ($note <= 2){
            $return = array(1,1,0,0,0);
            return $return;
        }elseif ($note <= 3){
            $return = array(1,1,1,0,0);
            return $return;
        }elseif ($note <= 4){
            $return = array(1,1,1,1,0);
            return $return;
        }elseif ($note <= 5){
            $return = array(1,1,1,1,1);
            return $return;
        }
    }
}
function putStarOnOrOff($bool){
    if ($bool == 1){
        return "class=\"btn btn-warning btn-xs\"";
    }else {
        return "class = 'btn btn-default btn-grey btn-xs'";
        }
}




/************************************************************************/
/************************************************************************/




/********************SUPPRESSION**************************/
/************************************************************************/
/*
 * Pour la suppression d'une publication : 3 étapes !
1) Suppression de la table "Publie" du lien entre le compte et la publication
2) MODIFICATION de la table "Ecrit" On enleve le lien de la publication SANS supprimer la ligne
Politique choisie de suppression (Voir le doc pour plus d'info)
2.5) On CONSERVE les commentaires
3) Suppression de la publication
*/

function deletePublicationPublie($publication_id, PDO $bdd = null){
    $nbModifs = 0;
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try{
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "DELETE FROM Publie WHERE Oeuvre_ID = :pPublication";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pPublication', $publication_id);
            $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function deletePublicationEcrit($publication_id, PDO $bdd = null){
    $nbModifs = 0;
    $null = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try{
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "UPDATE Ecrit SET Oeuvre_ID = NULL WHERE Oeuvre_ID = :pPublication";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pPublication', $publication_id);
        $stmt->execute();
        }
        catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
    return $nbModifs;
}

function deletePublication($publication_id, PDO $bdd = null){
    $nbModifs = 0;
    $result = null;
    if ($bdd == null) {
        $bdd = getDataBase();
    }
    // La bd est-elle valide ?
    if ($bdd) {
        try{
        // connexion réussie
        // 3 - On récupère toutes les données de l'éditeur
        $query = "DELETE FROM Oeuvre WHERE Oeuvre_ID = :pPublication";
        $stmt = $bdd->prepare($query);
        $stmt->bindParam(':pPublication', $publication_id);
        $stmt->execute();
    }
catch (Exception $e)
        {
            $nbModifs = 0;
        }
    }
return $nbModifs;
}
