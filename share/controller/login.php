<?php
// On démarre les sessions
session_start();

// Le modèle
require("../../model/model.php");

$error = 0;

if (isset($_POST["login"])) {
    // 1 - On récupère l'id du formulaires
    $login = htmlspecialchars($_POST['login']);
    $password = htmlspecialchars($_POST['password']);
    $redirect = isset($_GET['redirect']) ?  htmlspecialchars($_GET['redirect']) : "";
    if (isset($_POST['redirect'])) {
        $redirect = isset($_POST['redirect']) ? htmlspecialchars($_POST['redirect']) : "";
    }
    $user = authenticateUser($login, $password);
    if ($user)
    {
        $_SESSION["loginUser"] = $login;
        $_SESSION["roleUser"] = $user->role;
        if (!empty($redirect))
        {
            header("Location: " . $redirect);
        } elseif ($user->role == 100) {
            header("Location: ../../admin/controller/index.php");
        } else {
            header("Location: ../../client/controller/index.php");
        }
    }
    else {
        $error = 1;
        // La vue
        require ("../../share/view/login.php");
    }
}
else
{
    // La vue
    require ("../../share/view/login.php");
}

