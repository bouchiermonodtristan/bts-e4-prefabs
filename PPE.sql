-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 03, 2018 at 01:47 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `PPE`
--

-- --------------------------------------------------------

--
-- Table structure for table `Commentaire`
--

CREATE TABLE `Commentaire` (
  `Commentaire_ID` int(11) NOT NULL,
  `Commentaire_Titre` varchar(25) NOT NULL,
  `Commentaire_DatePublication` datetime DEFAULT NULL,
  `Commentaire_Contenu` varchar(1000) NOT NULL,
  `Commentaire_Note` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Commentaire`
--

INSERT INTO `Commentaire` (`Commentaire_ID`, `Commentaire_Titre`, `Commentaire_DatePublication`, `Commentaire_Contenu`, `Commentaire_Note`) VALUES
(1, 'Awesome !', '2018-04-30 00:00:00', 'This prefab changed my life ! I love it !', 4),
(2, 'Fantastic !', '2018-04-30 00:00:00', 'Seriously I really like stuff like that', 5),
(3, 'Hmmm...', '2018-04-30 00:00:00', 'Wonder how I could use this perle !', 4),
(4, 'YEAY !', '2018-04-30 00:00:00', 'Finally someone made it ! YES ! Oh god you are the very best !', 4),
(32, 'Super !', '2018-05-01 18:42:02', 'Niquel !', 5),
(36, 'Hihi, rainbow <3', '2018-05-01 20:05:50', 'Rainbow ! More and more rainbow !', 5),
(37, 'Sweet', '2018-05-01 22:06:47', 'Yes !', 3),
(38, 'Thank you !', '2018-05-02 05:35:07', 'Another one, perfect !', 4),
(39, 'Bouuuh', '2018-05-02 09:00:04', 'Comment hereça pue la merde !', 0),
(40, 'Titleaa', '2018-05-03 07:32:52', 'Comment here', 2),
(41, 'Titlea', '2018-05-03 07:53:10', 'Comment haere', 0),
(42, 'Title', '2018-05-03 07:54:58', 'Comment here', 0),
(43, 'Title', '2018-05-03 07:56:33', 'Comment here', 0),
(44, 'Title', '2018-05-03 07:58:28', 'Comment herea', 0),
(45, 'Title', '2018-05-03 08:04:55', 'Comment here', 0),
(46, 'Title', '2018-05-03 08:06:36', 'Comment here', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Compte`
--

CREATE TABLE `Compte` (
  `Compte_Pseudo` varchar(25) NOT NULL,
  `Compte_ID` int(11) NOT NULL,
  `Compte_DateInscription` datetime DEFAULT NULL,
  `Compte_Password` varchar(32) DEFAULT NULL,
  `Compte_DateDernierMessage` datetime DEFAULT NULL,
  `Compte_IsAdmin` tinyint(1) DEFAULT NULL,
  `Compte_Description` varchar(500) DEFAULT NULL,
  `Compte_Image` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Compte`
--

INSERT INTO `Compte` (`Compte_Pseudo`, `Compte_ID`, `Compte_DateInscription`, `Compte_Password`, `Compte_DateDernierMessage`, `Compte_IsAdmin`, `Compte_Description`, `Compte_Image`) VALUES
('test', 1, '2018-04-30 00:00:00', 'test', '2018-05-03 08:06:36', 0, 'I love hard banana !', 'https://public-media.smithsonianmag.com/filer/2a/29/2a299fcc-1bbb-47c0-b0f1-2d349c848ded/pia00407-16.jpg'),
('Dieu', 2, '2018-04-30 00:00:00', 'test', '2018-04-30 00:00:00', 0, 'Salut', 'https://seeklogo.com/images/U/unity-logo-988A22E703-seeklogo.com.png'),
('DarkVader', 3, '2018-04-30 00:00:00', 'test', '2018-05-02 09:00:04', 0, 'May the force be your !', 'https://pbs.twimg.com/profile_images/743107339/darth-vader-face_400x400.jpg'),
('Obiwan', 4, '2018-04-30 00:00:00', 'test', '2018-04-30 00:00:00', 0, 'I dare you to beat me with a lightsaber', 'https://pmcvariety.files.wordpress.com/2017/08/obi-wan-kenobi.jpg?w=1000&h=562&crop=1'),
('Wolf', 5, '2018-05-03 02:09:43', 'test', NULL, NULL, 'I am a wolf among men !\r\nAhouuu !', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQjGr9dryqG09XzgH9ynkyt-0ZHiIksBR3meAQ2ISCFmP9YiS9R');

-- --------------------------------------------------------

--
-- Table structure for table `Ecrit`
--

CREATE TABLE `Ecrit` (
  `Compte_ID` int(11) NOT NULL,
  `Commentaire_ID` int(11) NOT NULL,
  `Oeuvre_ID` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Ecrit`
--

INSERT INTO `Ecrit` (`Compte_ID`, `Commentaire_ID`, `Oeuvre_ID`) VALUES
(1, 45, NULL),
(1, 46, NULL),
(1, 32, 1),
(3, 1, 1),
(4, 2, 1),
(1, 37, 2),
(3, 3, 2),
(1, 38, 3),
(3, 39, 3),
(4, 4, 3),
(1, 36, 5);

-- --------------------------------------------------------

--
-- Table structure for table `Oeuvre`
--

CREATE TABLE `Oeuvre` (
  `Oeuvre_ID` int(11) NOT NULL,
  `Oeuvre_DateCreation` datetime NOT NULL,
  `Oeuvre_Titre` varchar(25) NOT NULL,
  `Oeuvre_Genre1` varchar(25) DEFAULT NULL,
  `Oeuvre_Genre2` varchar(25) DEFAULT NULL,
  `Oeuvre_Genre3` varchar(25) DEFAULT NULL,
  `Oeuvre_Type` varchar(25) DEFAULT NULL,
  `Oeuvre_Contenu` varchar(1000) DEFAULT NULL,
  `Oeuvre_Image` varchar(100) DEFAULT NULL,
  `Commentaire_ID` int(11) DEFAULT NULL,
  `Oeuvre_Telechargement` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Oeuvre`
--

INSERT INTO `Oeuvre` (`Oeuvre_ID`, `Oeuvre_DateCreation`, `Oeuvre_Titre`, `Oeuvre_Genre1`, `Oeuvre_Genre2`, `Oeuvre_Genre3`, `Oeuvre_Type`, `Oeuvre_Contenu`, `Oeuvre_Image`, `Commentaire_ID`, `Oeuvre_Telechargement`) VALUES
(1, '2018-04-30 00:00:00', 'CameraSelfie On Screen', 'VRChat', NULL, NULL, 'Prefab', 'Camera on pickable pad with screen on it + Big Screen TV linked to it + Respawn timer on drop on a respawn gameobject', 'https://cdn.discordapp.com/attachments/408291774923276297/441520527509684225/Camera4.PNG', NULL, 'https://drive.google.com/open?id=1qbqN4jlp00SIBnXguaTBxWcsFxmHEndA'),
(2, '2018-04-30 00:00:00', 'Flipped Sphere.', 'VRChat', NULL, NULL, 'Prefab', 'Flipped Sphere. Use it to create several skybox in the same world !', 'https://cdn.discordapp.com/attachments/408291774923276297/441520536955256832/Far3.PNG', NULL, 'https://drive.google.com/open?id=1-wEQRe1dd3gO1TO7c-vpQrE2V9vc9iyR'),
(3, '2018-04-30 00:00:00', 'MovableSkyboxSphere', 'VRChat', NULL, NULL, 'Prefab', 'A pickup creating a movable skybox. Turn it in everydirection. Good effect with DAY/NIGHT material on the flipped sphere. Layer can be set on local to not importunate in crowded worlds', 'https://cdn1.vox-cdn.com/uploads/chorus_asset/file/8402075/941450_609208285758470_875871287_n.0.png', NULL, 'https://drive.google.com/open?id=1zOU-MDJBWCUkI1YirUQGrulaRPhy73OU'),
(4, '2018-04-30 00:00:00', 'VRC Video Player', 'VRChat', NULL, NULL, 'Prefab', 'A player multitrack for your songs, with plalist and buttons to toggle !', 'https://lh3.ggpht.com/8BDSUv0P-kr9Kik7weOSet4Au_BlBpM0SmM8dUDdYQOceErGCLroJlhVoSL7cPP14YE', NULL, 'https://drive.google.com/open?id=16_St7_IHfSo0D6TbtXePmTUVouHIet_u'),
(5, '2018-04-30 00:00:00', 'RainbowTrailWand', 'VRChat', NULL, NULL, 'Prefab', 'Rainbow trail marker wand', 'https://image.freepik.com/free-vector/magic-wand-with-yellow-trail-background_1278-79.jpg', NULL, 'https://drive.google.com/open?id=1G_8eEpkxAz73dP4xgOG39h1eyhbu6vz1'),
(15, '2018-05-02 11:57:36', 'Chest Prefab !', 'VRChat', NULL, NULL, 'Prefab', 'A transportable Chest.\r\n24 item, 999qty inventory chest system\r\n\r\nSDK version : 2018.04.25.09.19', 'https://images-na.ssl-images-amazon.com/images/I/915PBmxDsWL._SL1500_.jpg', NULL, 'https://drive.google.com/file/d/1_I5FU0TetTCkpuqZC8S8poDGxVsciHu8/view'),
(17, '2018-05-03 02:14:53', 'Another Flipped Sphere', 'Unity', NULL, NULL, 'Model', 'Inverted sphere with a cubemap shader for it, so you can have seprate skyboxes in areas', 'https://forums.unrealengine.com/filedata/fetch?id=1208695&amp;d=1485965452', NULL, 'https://drive.google.com/open?id=1r30MU4wlRLeid2SJU-CZo0xpBUUUDyCz'),
(20, '2018-05-03 07:58:25', 'a', 'Unity', NULL, NULL, 'Prefab', 'a', 'a', NULL, 'a');

-- --------------------------------------------------------

--
-- Table structure for table `Publie`
--

CREATE TABLE `Publie` (
  `Compte_ID` int(11) NOT NULL,
  `Oeuvre_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Publie`
--

INSERT INTO `Publie` (`Compte_ID`, `Oeuvre_ID`) VALUES
(2, 1),
(2, 2),
(2, 3),
(1, 4),
(1, 5),
(1, 15),
(5, 17);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Commentaire`
--
ALTER TABLE `Commentaire`
  ADD PRIMARY KEY (`Commentaire_ID`);

--
-- Indexes for table `Compte`
--
ALTER TABLE `Compte`
  ADD PRIMARY KEY (`Compte_ID`),
  ADD UNIQUE KEY `Compte_Pseudo` (`Compte_Pseudo`);

--
-- Indexes for table `Ecrit`
--
ALTER TABLE `Ecrit`
  ADD PRIMARY KEY (`Compte_ID`,`Commentaire_ID`),
  ADD KEY `FK_Ecrit_Commentaire_ID` (`Commentaire_ID`),
  ADD KEY `FK_Ecrit_Oeuvre_ID` (`Oeuvre_ID`);

--
-- Indexes for table `Oeuvre`
--
ALTER TABLE `Oeuvre`
  ADD PRIMARY KEY (`Oeuvre_ID`),
  ADD KEY `FK_Oeuvre_Commentaire_ID` (`Commentaire_ID`);

--
-- Indexes for table `Publie`
--
ALTER TABLE `Publie`
  ADD PRIMARY KEY (`Compte_ID`,`Oeuvre_ID`),
  ADD KEY `FK_Publie_Oeuvre_ID` (`Oeuvre_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Commentaire`
--
ALTER TABLE `Commentaire`
  MODIFY `Commentaire_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `Compte`
--
ALTER TABLE `Compte`
  MODIFY `Compte_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Oeuvre`
--
ALTER TABLE `Oeuvre`
  MODIFY `Oeuvre_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Ecrit`
--
ALTER TABLE `Ecrit`
  ADD CONSTRAINT `FK_Ecrit_Commentaire_ID` FOREIGN KEY (`Commentaire_ID`) REFERENCES `Commentaire` (`Commentaire_ID`),
  ADD CONSTRAINT `FK_Ecrit_Compte_ID` FOREIGN KEY (`Compte_ID`) REFERENCES `Compte` (`Compte_ID`),
  ADD CONSTRAINT `FK_Ecrit_Oeuvre_ID` FOREIGN KEY (`Oeuvre_ID`) REFERENCES `Oeuvre` (`Oeuvre_ID`);

--
-- Constraints for table `Oeuvre`
--
ALTER TABLE `Oeuvre`
  ADD CONSTRAINT `FK_Oeuvre_Commentaire_ID` FOREIGN KEY (`Commentaire_ID`) REFERENCES `Commentaire` (`Commentaire_ID`);

--
-- Constraints for table `Publie`
--
ALTER TABLE `Publie`
  ADD CONSTRAINT `FK_Publie_Compte_ID` FOREIGN KEY (`Compte_ID`) REFERENCES `Compte` (`Compte_ID`),
  ADD CONSTRAINT `FK_Publie_Oeuvre_ID` FOREIGN KEY (`Oeuvre_ID`) REFERENCES `Oeuvre` (`Oeuvre_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
